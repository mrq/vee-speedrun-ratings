## Requirements

* Python3.x
* `matplotlib`

## Usage

```
# Example with queue.json
python3 chart.py --fetch

# Example with markers without queue.json
python3 chart.py --fetch --name "Run Name" --url "Thread URL" --markers "overestimate,trainwreck"

# Plot
python3 chart.py --plot

# Fetch + Plot
python3 chart.py --fetch --plot

# Print Additional flags
python3 chart.py --help
```

## Attribution

Credit to the original [chart anon](https://arch.b4k.co/v/thread/604205468/#604205596) for the original scripts. I merely just adapted it over the years for my ease-of-use.